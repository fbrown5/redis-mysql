import sys
import time

from mysql.connector import connection

"""python LocalMySQLQuery.py host user password database times"""
if len(sys.argv) != 6:
    print("invalid number of arguments")
    exit
else:
    config = {
        'user': sys.argv[2],
        'password': sys.argv[3],
        'host': sys.argv[1],
        'database': sys.argv[4],
        'raise_on_warnings': True
    }
    query = ("select * from gold")

    times = int(sys.argv[5])
    successfulQueries = 0
    totalTime = 0.0

    try:
        connect = connection.MySQLConnection(**config)
        cursor = connect.cursor(buffered=True)

        for x in range(0, times):
            startTime = time.time()
            cursor.execute(query)
            duration = time.time() - startTime
            if cursor.rowcount > 0:
                successfulQueries += 1
                totalTime += duration

        print("Total Time: %s Number of Successful Queries: %5d" % (totalTime, successfulQueries))
    except Exception as e:
        print("error %s" %(e)) 
    else:
        connect.close()
