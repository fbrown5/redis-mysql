#!/bin/bash
for i in {1..10000}
do
	echo "SET $i $i" >> redisData.txt
done

cat redisData.txt | redis-cli --pipe
rm redisData.txt
