import redis
import sys
import time

redis_host = "localhost"
redis_port = 6379
redis_password = ""

"""python LocalRedisQuery.py times"""
if len(sys.argv) != 2:
    print("invalid number of arguments")
    exit
else:
    times = int(sys.argv[1])
    totalTime = 0.0
    try:
        r = redis.StrictRedis(host=redis_host, port=redis_port, password=redis_password, decode_responses=True)

        for x in range(1, times):
            startTime = time.time()
            response = r.get(x)
            duration = time.time() - startTime
            totalTime += duration


    except Exception as e:
        print("error")
        print(e)

    else:
        print("Total time: %5.4f" % (totalTime))