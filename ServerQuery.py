import sys
import requests
import time
from bs4 import BeautifulSoup

"""python ServerQuery.py times"""

if len(sys.argv) != 2:
    print("invalid number of arguments")
    exit
else:
    times = int(sys.argv[1])
    redisTotalTime = 0.0
    mysqlTotalTime = 0.0
    totalTime = 0.0
    redisTimes = 0
    mySQLTimes = 0

    for x in range(0, times):
        startTime = time.time()
        requestResponse = requests.get('http://localhost/database-ui.php?query=select+*+from+gold')
        duration = time.time() - startTime
        parsedHTML = BeautifulSoup(requestResponse.text, features="html.parser")
        parsed = parsedHTML.find_all("h2")[0]

        if "Redis" in str(parsed.string):
            redisTotalTime += duration
            redisTimes += 1
        elif "MySQL" in str(parsed.string):
            mysqlTotalTime += duration
            mySQLTimes += 1
        else:
            print("Neither MySQL nor Redis")
        totalTime += duration

    print("Total Time: %10.4f Redis Query Count: %5d Redis Average time: %10.4f" % (
    totalTime, redisTimes, redisTotalTime/redisTimes))
    if mySQLTimes > 0:
        print("MySQL Query Count: %5d MySQL Average time: %10.4f" % ( mySQLTimes, mysqlTotalTime/mySQLTimes))
    else:
        print("There were no MySQL Queries.")
    