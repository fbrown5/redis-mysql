# CS4411a Group Project
## Comparing Redis to MySQL

### Testing MySQL
* Download and install MySQL.
* Copy server/monthly_csv.csv to the secure file location. Mine was C:\ProgramData\MySQL\MySQL Server 8.0\Uploads
* By default it installs into drive c: which in my case was a SSD. 
* To test on other drives copy data folder from C:\ProgramData\MySQL\MySQL Server 8.0\Data to wherever you'd like.
* Change the datadir in my.ini within C:\ProgramData\MySQL\MySQL Server 8.0 to where you pasted it. 
* You will have to restart your MySQL server to apply the changes. 
* Both times you can use the LocalMySQLQuery.py class to repeat the same query over and over again and records time for each. 

### Testing Redis
* I utilized WSL Ubuntu which I attained through the Microsoft Store.
* Run the LoadRedis.sh script to populate Redis very plainly. 
* You can then run the LocalRedisQuery.py with the number of times you want it to query up to 10,000.

### Testing Redis 
 * Run build_server.bat to run container and fill with data. Container will stay live for 1 day or until stopped.
 * database.php is a simple php page and database-ui.php has an interface. 
 * you can hit database.php with http requests like:
   * curl -v http://localhost/database.php?query=select+*+from+gold+where+Price+%3E+1400.00
 * Alternatively run ServerQuery.py with the number of times to query this webpage.
 * The page first checks to see if the value is in Redis and if it is returns. 
 * If it is not present in Redis it will then check MySQL and then insert it into Redis.