<?php         
    if (isset($_GET['query'])) {
        $end_time = 0;
        try {
            $sql = $_GET['query'];

            $data_source = '';

            $redis = new Redis(); 
            $redis->connect('127.0.0.1', 6379); 

            // $sql = 'select * from gold where Price > 1400.00';

            $cache_key = md5($sql);
            $start_time = microtime(true);

            if ($redis->exists($cache_key)) {
                $end_time = (microtime(true) - $start_time);
                $data_source = "Data from Redis Server";
                $data = unserialize($redis->get($cache_key));

            } else {
                $end_time = (microtime(true) - $start_time);

                $data_source = 'Data from MySQL Database';

                $db_name     = 'testdb';
                $db_user     = 'testuser';
                $db_password = 'password';
                $db_host     = 'localhost';

                $pdo = new PDO('mysql:host=' . $db_host . '; dbname=' . $db_name, $db_user, $db_password);
                $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                $stmt = $pdo->prepare($sql);
                $stmt->execute();
                $data = []; 

                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {          
                $data[] = $row;  
                }  

                $redis->set($cache_key, serialize($data)); 
                $redis->expire($cache_key, 10000);        
        }
        echo sprintf("%s in :%.6f s", $data_source , $end_time);

        } catch (PDOException $e) {
            echo 'Database error. ' . $e->getMessage();
        }
    }
?>