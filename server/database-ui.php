<html>
	<head>
		<title>Using Redis Server with PHP and MySQL</title>
		<style>
			INPUT {
				height: 30px;
				width: 100%;
			}
		</style>
	</head> 
	<body>
	<?php 
		if (isset($_GET['query'])) {
			$sql = $_GET['query'];
		} else {
			$sql="";
		}
	?>
		
	<FORM NAME ="form1" METHOD ="GET" ACTION = "database-ui.php">
		<INPUT TYPE = "TEXT" NAME = "query" VALUE ="<?php print$sql;?>">
		<INPUT TYPE = "Submit" Name = "Submit1" VALUE = "Run Query">
  	</FORM> 

    <h1 align = 'center'>Price's of Gold</h1>

    <table align = 'center' border = '2'>        

    <?php 
        
		if (isset($_GET['query'])) {
			$end_time = 0;
			try {
				$sql = $_GET['query'];
				print($sql);

				$data_source = '';

				$redis = new Redis(); 
				$redis->connect('127.0.0.1', 6379); 

				// $sql = 'select * from gold where Price > 1400.00';

				$cache_key = md5($sql);
				$start_time = 0;

				if ($redis->exists($cache_key)) {
					$data_source = "Data from Redis Server";
					$start_time = microtime(true)
					$redis->get($cache_key)
					$end_time = (microtime(true) - $start_time)*1000;
					$data = unserialize($redis->get($cache_key));

				} else {
					$data_source = 'Data from MySQL Database';

					$db_name     = 'testdb';
					$db_user     = 'testuser';
					$db_password = 'password';
					$db_host     = 'localhost';

					$pdo = new PDO('mysql:host=' . $db_host . '; dbname=' . $db_name, $db_user, $db_password);
					$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

					$stmt = $pdo->prepare($sql);
					$start_time = microtime(true)
					$stmt->execute();
					$end_time = (microtime(true) - $start_time)*1000;
					$data = []; 
					

					while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {          
					$data[] = $row;  
					}  

					$redis->set($cache_key, serialize($data)); 
					$redis->expire($cache_key, 10);        
			}

			$elapsed_time = sprintf("Response Time: %1\$.4f", $end_time);

			echo "<tr><td colspan = '2' align = 'center'><h2>$data_source</h2></td></tr>";
			echo "<tr><td colspan = '2' align = 'center'><h2>$elapsed_time</h2></td></tr>";
			echo "<tr><th>Year-Month</th><th>Price</th></tr>";

			foreach ($data as $record) {
				echo '<tr>';
				echo '<td>' . $record['yearmonth'] . '</td>';
				echo '<td>' . $record['price'] . '</td>';
				echo '</tr>'; 
			}              


			} catch (PDOException $e) {
				echo 'Database error. ' . $e->getMessage();
			}
		} else {
			$sql="";
		}
   	?>

		</table>
	</body>
</html>