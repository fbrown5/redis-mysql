CREATE DATABASE IF NOT EXISTS testdb;
CREATE USER IF NOT EXISTS 'testuser'@'localhost' IDENTIFIED WITH mysql_native_password BY 'password';
GRANT ALL PRIVILEGES ON testdb.* TO 'testuser'@'localhost';
FLUSH PRIVILEGES;
USE testdb;

DROP TABLE IF EXISTS gold;
CREATE TABLE IF NOT EXISTS gold
(
    yearmonth varchar(10),
    price DECIMAL(10,3),
    PRIMARY KEY (yearmonth)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOAD DATA INFILE '/var/lib/mysql-files/monthly.csv' INTO TABLE gold FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' IGNORE 1 ROWS;

SELECT * FROM gold WHERE yearmonth > '2020-00';